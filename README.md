# ReadMe

This project is only designed to see if your maven settings file is correctly set up with your artifactory credentials. To set up 
your maven settings file, please follow the instructions at: https://termlex.atlassian.net/wiki/spaces/PROD/pages/164298810/Maven+settings+file

### Running locally

- clone the repo
- Run `mvn clean compile`

If your settings are right, then the project should be successfully built by maven.