# Git Changelog for parent-pom-test
Changelog auto generated from Git commits.
## 1.6.0-NI
### No issue

[ee0b47d10df1f04](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/ee0b47d10df1f04) Jay Kola *2015-03-31 11:19:54*

Case 926: INPROGRESS : Simplified delete logic and flash messages

[ad930fdafd1c2c5](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/ad930fdafd1c2c5) Jay Kola *2015-03-31 10:14:20*

Case 926: INPROGRESS : Added plugin file updated by maven

[4db907a1987ec44](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/4db907a1987ec44) Jay Kola *2015-03-31 10:08:03*

Case 926: INPROGRESS : Updated delete method to handle html and json requests


## 1.5.0-NI
### No issue

[198c9f37f17f617](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/198c9f37f17f617) Jay Kola *2015-02-27 20:53:57*

Case 896: INPROGRESS : Added search methods for invitations and known domains

[1dc33d0c8584749](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/1dc33d0c8584749) Jay Kola *2015-02-11 13:11:43*

Added files updated by Maven release


## 1.4.0-NI
### No issue

[fb68b85bbec2ff3](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/fb68b85bbec2ff3) Jay Kola *2015-02-11 11:51:17*

Case 866: INPROGRESS : Added functionality to revoke invited users

[76e37e2952f3d5d](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/76e37e2952f3d5d) Jay Kola *2015-02-11 10:56:56*

Case 856: INPROGRESS : Changed notification mail type from plain text to html

[b7cc0f2f9800f44](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/b7cc0f2f9800f44) Jay Kola *2015-02-11 10:54:33*

Case 865: INPROGRESS : Deleting known domain now handles ids passed as string

[02b45ac3062149e](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/02b45ac3062149e) Jay Kola *2015-02-11 10:09:18*

Case 856: INPROGRESS : Admin notifications now use a gsp template


## 1.3.0-NI
### No issue

[b44b5c3318109b1](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/b44b5c3318109b1) Jay Kola *2015-02-06 21:44:09*

Case 856: INPROGRESS : Updated plugin to include server url in notification emails. Request notifications now use a gsp template

[79fc50cd29fe346](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/79fc50cd29fe346) Jay Kola *2014-10-19 16:30:37*

Case 806: INPROGRESS : Updated map to access approval list and count via the new map object returned by controller

[e686c4d95dcdaa9](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/e686c4d95dcdaa9) Jay Kola *2014-09-29 16:24:37*

Updated plugin to use Noesis grails POM

[5a9287b37333b11](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/5a9287b37333b11) Jay Kola *2014-05-19 12:55:46*

Adding files updated by Maven

[32aba77730fd0f2](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/32aba77730fd0f2) Jay Kola *2014-05-19 12:55:34*

Added tag Start of version 1.3.0 development for changeset cab71ecdf6e0


## 1.2.0-NI
### No issue

[4eb3d5b8a3541d9](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/4eb3d5b8a3541d9) Jay Kola *2014-05-16 11:39:13*

case 707: INPROGRESS: List methods no longer restrict domain items to just 10. Cleaned up commented code

[11544786d8b1077](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/11544786d8b1077) Jay Kola *2014-05-16 08:31:57*

case 707: INPROGRESS: Added default messages.properties

[2bc9bae4fbd996b](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/2bc9bae4fbd996b) Jay Kola *2014-05-15 23:17:03*

case 707: INPROGRESS: Invitation service and controller updated to use more generic params type parameters

[fb964b29fb4d049](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/fb964b29fb4d049) Jay Kola *2014-05-14 21:33:56*

case 707: INPROGRESS: Invitation template tweaked with correct spacing between lines

[b30bb8f65cafcff](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/b30bb8f65cafcff) Jay Kola *2014-05-14 21:33:30*

case 707: INPROGRESS: InvitationService now exposes init method with public access - previous private access was causing issues with service initialisation & service was no longer transactional

[ca9493df4c0c5cb](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/ca9493df4c0c5cb) Jay Kola *2014-05-14 21:32:13*

case 707: INPROGRESS: Fixed issue with controller which was trying to return a boolean value directly as a JSON object - needs to be wrapped in map or list

[70ff4b37816dfae](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/70ff4b37816dfae) Jay Kola *2014-05-09 08:58:10*

case 707: INPROGRESS: Invitations list method now returns approved, unapproved and all invitation objects

[52e822c9e98a9b2](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/52e822c9e98a9b2) Jay Kola *2014-05-09 07:23:23*

case 707: INPROGRESS: Invitation template now sends handles invite to Snolex differently to any other group.

[383c0a5a79498e3](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/383c0a5a79498e3) Jay Kola *2014-05-08 22:35:32*

case 707: INPROGRESS: InvitationController methods updated for ease of use - approvals and revokes can be performed directly now

[c1171f61425aafe](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/c1171f61425aafe) Jay Kola *2014-05-08 22:09:04*

case 707: INPROGRESS: Added controller for known domains and associated tests

[a42286efd75e3a2](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/a42286efd75e3a2) Jay Kola *2014-05-08 18:01:12*

case 707: INPROGRESS: Users from known domains now automatically get emailed using specified email template

[218d01b27e3389e](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/218d01b27e3389e) Jay Kola *2014-05-08 17:10:36*

case 707: INPROGRESS: Invitations from users in known domains automatically get approved

[38c8b1905b2746c](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/38c8b1905b2746c) Jay Kola *2014-05-08 16:32:57*

case 707: INPROGRESS: Added group name as attribute for known domain class

[1e88f5d19ee791d](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/1e88f5d19ee791d) Jay Kola *2014-05-08 16:10:48*

case 707: INPROGRESS: Added KnownDomains a domain class that is used by InvitaitonService to track list of known domains. Corresponding tests added

[e1d0a289f176dc8](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/e1d0a289f176dc8) Jay Kola *2013-12-24 10:28:27*

Added tag Start of version 1.2.0 development for changeset 3f5a1dfb3fe6


## Start_of_version_1.2.0_development
### No issue

[1e7481dacc7911b](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/1e7481dacc7911b) Jay Kola *2013-12-24 10:28:12*

Adding files updated by Maven


## 1.1.1-NI
### No issue

[15e7aab371d6d98](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/15e7aab371d6d98) Jay Kola *2013-12-24 10:23:30*

Added tag Before release of version 1.1.1-NI for changeset a127abab8294


## Before_release_of_version_1.1.1-NI
### No issue

[8c31c93c58c31a6](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/8c31c93c58c31a6) Jay Kola *2013-12-24 10:21:37*

case 595: INPROGRESS: Downgraded grails version to 1.3.1 since the plugin doesn't need 2.2 as minimum

[f3ff41e7580f7f6](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/f3ff41e7580f7f6) Developer Noesis Informatica *2013-12-24 10:18:25*

Added files that were previous missed in hg add… Ignored plugin file.

[c0050f9e377e7b6](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/c0050f9e377e7b6) Jay Kola *2013-12-23 23:33:31*

case 595: INPROGRESS: Issue fixed by changing the global adminAddress and serverAddress variables into local ones.

[26dabc7b8227828](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/26dabc7b8227828) Jay Kola *2013-12-23 13:19:17*

case 592: FIXED: Issue was being caused by improper intialisation of variables in class. Initialisation has now been moved to post-construct method

[46a64b447649636](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/46a64b447649636) Jay Kola *2013-12-23 12:25:01*

Updated Data source definition to use H2

[b2a4d47505ad7ed](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/b2a4d47505ad7ed) Jay Kola *2013-12-23 12:24:28*

Updated plugin groovy definition to match Maven artefact and Grails version

[92b16c7a754fa1c](https://github.com/noesisinformatica/noesisinformatica/parent-pom-test/commit/92b16c7a754fa1c) Jay Kola *2013-12-23 12:23:44*

Added pom for project with correct settings for artefact and scm


